#!/usr/bin/perl
use Data::Dumper;
my $urls = {};
my $this_cafe = '';

while(<>) {
    if (/search\?q\=(.+?)\&count/) {
        $this_cafe = $1;
        warn $this_cafe;
    }
    my ($url) = /\"displayUrl\"\: \"(.+?)\"/;
    if ($url) {
        $urls->{$this_cafe} = $url;
    }
}

print Dumper $urls;