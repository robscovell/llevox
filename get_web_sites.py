# -*- coding: utf-8 -*-
"""
Second step is to use Google to get the websites for the coffee shops from the data in the db
"""
import MySQLdb
import urllib2
import time
import re

bingSearchAPIKey = "43da19899a9f4c91bbb368f26cf003a3"
bingUrl = "https://api.cognitive.microsoft.com/bing/v5.0/search?q=%s&count=1"
query = "SELECT name, city, country FROM coffee_shops WHERE id_coffee_shops > 617"

db = MySQLdb.connect(
    read_default_group='Llevox',
    db='llevox'
)

with db as cursor:
    cursor.execute(query)
    cafes = cursor.fetchall()

for cafe in cafes:
    searchTerm = "+".join(cafe)
    searchTerm = re.sub(r'\s+', "+", searchTerm)
    url = bingUrl % searchTerm
    print("!!!Processing URL " + url + "!!!")
    req = urllib2.Request(url)
    req.add_header('Ocp-Apim-Subscription-Key', bingSearchAPIKey)
    resp = urllib2.urlopen(req)
    content = resp.read()
    print(content)
    time.sleep(1)
