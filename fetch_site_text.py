# Now we need to fetch the text for each site and put it in a TEXT field in the DB

from bs4 import BeautifulSoup
import MySQLdb
import urllib2
import re

query = "SELECT id_coffee_shops, url FROM coffee_shops WHERE training_data = 'Y' AND url IS NOT NULL AND soup_text IS NULL"
insert_query = u"UPDATE coffee_shops SET soup_text = '%s' WHERE id_coffee_shops = %s"

db = MySQLdb.connect(
    read_default_group='Llevox',
    db='llevox'
)

with db as cursor:
    cursor.execute(query)
    urls = cursor.fetchall()
    for url in urls:
        url_link = url[1]
        url_link = re.sub(r'http\:\/\/', "", url_link)
        url_link = re.sub(r'https\:\/\/', "", url_link)
        url_link = 'http://' + url_link
        print("Processing " + url_link)
        try:
            req = urllib2.Request(url_link)
            resp = urllib2.urlopen(req)
            content = resp.read()
            soup = BeautifulSoup(content, 'html.parser')

            # kill all script and style elements
            for script in soup(["script", "style"]):
                script.extract()    # rip it out            

            fulltext = soup.get_text()
            fulltext = re.sub(r'\s+', ' ', fulltext)
            fulltext = re.sub(r"\'", '', fulltext)
            fulltext = u''.join((fulltext))
            with db as cursor2:
                cursor2.execute(insert_query % (fulltext, url[0]))
                db.commit()
        except Exception as e:
            print e
