# -*- coding: utf-8 -*-
# pylint: disable=C0103
"""
First step is to parse the kml file and write the cafes and their locations to the database.
"""
import xmltodict
import MySQLdb


cafes = []

with open('2wc.kml') as fd:
    kml = xmltodict.parse(fd.read())

for folder in kml['kml']['Document']['Folder']:
    print(folder['name'])
    for cafe in folder['Placemark']:
        name = cafe['name']
        xdata = cafe['ExtendedData']
        country = xdata['Data'][0]['value']
        city = xdata['Data'][1]['value']
        gps = cafe['Point']['coordinates']
        cafes.append({'name': name, 'city': city, 'country': country, 'gps': gps})

db = MySQLdb.connect(
    read_default_group='Llevox',
    db='llevox'
)

insert_stmt = (
    "INSERT INTO coffee_shops (name, city, country, GPS, good, training_data) "
    "VALUES (%s, %s, %s, %s, %s, %s)"
)
with db as cursor:
    for cafe in cafes:

        data = (cafe['name'], cafe['city'], cafe['country'], cafe['gps'], 'Y', 'Y')
        cursor.execute(insert_stmt, data)
