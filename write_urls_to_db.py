# -*- coding: utf-8 -*-
# Now we need to write the extracted URLs to the DB

import MySQLdb
import json
import re

db = MySQLdb.connect(
    read_default_group='Llevox',
    db='llevox'
)

# Need to get the coffee shops first
select_stmt = ("SELECT id_coffee_shops, name, city, country FROM coffee_shops")

with db as cursor:
    cursor.execute(select_stmt)
    coffee_shops = cursor.fetchall()

# map the name to the id
coffee_shop_ids = {}

for coffee_shop in coffee_shops:
    coffee_shop_name = '+'.join((coffee_shop[1], coffee_shop[2], coffee_shop[3]))
    coffee_shop_name = re.sub(r'\s+', "+", coffee_shop_name)
    coffee_shop_name = re.sub("'", "", coffee_shop_name)
    print(coffee_shop_name)
    coffee_shop_name = coffee_shop_name.decode('utf-8')
    coffee_shop_ids[coffee_shop_name] = coffee_shop[0]

# Now import and parse the JSON
js = json.decoder
with open('display_urls.json') as fd:
    display_urls = json.loads(fd.read())
    update_stmt = (u"UPDATE coffee_shops SET url = '%s' WHERE id_coffee_shops = %s")
    for coffee_shop_name in display_urls:
        coffee_shop_name = re.sub(r'\"', "", coffee_shop_name)
        try:
            coffee_shop_id = coffee_shop_ids[coffee_shop_name]
            coffee_shop_url = display_urls[coffee_shop_name]
            with db as cursor:
                cursor.execute(update_stmt % (u''.join((coffee_shop_url)), coffee_shop_id))
        except Exception as e:
            print(e)
