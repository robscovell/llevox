Llevox, a coffee seeking scraper.

This bot was created for a Startup Weekend competition to search the web for fancy-schmancy third-wave coffee outlets worldwide, so that coffee snobs can arrive in any country and know where to go for the right shot to recover from their traumatic in-flight coffee experiences.